/** Created by:
 *  Πέτρος Γκετσόπουλος 3130041
 *  Κωνσταντίνος Ευθυμίου 3130060 */
package com.miners.tavligame;

/* To be used in array with info
 * about the placement of
 * the pieces for each player */

public class Pieces {

    private int player; // 1 for Light, -1 for Dark Player
    private int number; // of Pieces in each position

    //Constructors

    public Pieces(){
        this.player=0;
        this.number=0;
    }

    public Pieces(int Player, int Number){
        this.player = Player;
        this.number = Number;
    }

    public Pieces(Pieces copy){
        this.player = copy.player;
        this.number = copy.number;
    }

    // Getters

    public int getPlayer() {
        return player;
    }

    public int getNumber() {
        return number;
    }

    // Setters

    public void setPlayer(int player) {
        this.player = player;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    //Add/Remove Piece from position

    public void addPiece(){
        this.number++;
    }
    public void removePiece(){
        this.number--;
    }
}
