/** Created by:
 *  Πέτρος Γκετσόπουλος 3130041
 *  Κωνσταντίνος Ευθυμίου 3130060 */
package com.miners.tavligame;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.Window;
import android.os.Handler;

public class GameActivity extends AppCompatActivity{
    GameView game ;
    int xfrom, xto, yfrom, yto;
    static int counter =0;
    boolean valid = false;
    static Handler s0 = new Handler();
    static MediaPlayer dice;
    static MediaPlayer piece;
    static MediaPlayer endW;
    static MediaPlayer endL;
    static Thread t = new Thread();
    static int depth2 ;
    static int depth1 = depth2 = 2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        //MediaPlayers for each sound effect
        dice = MediaPlayer.create(getApplicationContext(), R.raw.dice);
        piece = MediaPlayer.create(getApplicationContext(), R.raw.piece);
        endW = MediaPlayer.create(getApplicationContext(), R.raw.win);
        endL = MediaPlayer.create(getApplicationContext(), R.raw.lose);

        //Create GameView object with screen's dimensions
        game = new GameView(this, dm.heightPixels, dm.widthPixels);
        setContentView(game);
        t = new Thread() {

            @Override
            public void run() {
                game.setPlayers(new Player(depth1), new Player(depth2));
                while (!game.b.gameOver()) {
                    try {
                        if(game.b.getNextPlayer()==1) {
                            if (!game.canPlay(1)) {//If player can't play, continue
                               // Thread.sleep(3000);
                                game.b.setNextPlayer(-1);
                                t.notify();
                                continue;
                            }
                            game.play();
                            s0.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    playSound(1);
                                }
                            }, 100);
                        } else if(game.b.getNextPlayer()==-1) {
                           // game.dark.rollDices();
                            if(!game.canPlay(-1)) {
                                Log.d("GA", "Can't Play!");
                                Thread.sleep(1000);
                                game.b.setNextPlayer(1);
                                continue;
                            }
                            try {
                                synchronized (this) {
                                    wait();
                                }
                            } catch (InterruptedException ex) {
                                Log.d("GA", "error in waiting!");
                            }
                        }
                        if(game.b.gameOver())
                            Log.d("GA", "OOOOOOOOVEEEEEEEEEEEEERRRRRRRRRRRRR");
                        Thread.sleep(4000);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        t.start();
    }

    //Depending on touch input, call the right methods from GameView
    @Override
    public boolean onTouchEvent(MotionEvent evt) {
        if(evt.getAction() == MotionEvent.ACTION_DOWN&&game.b.getNextPlayer()==-1) {
            if (!game.b.gameOver()) {
                if (counter == 0) {
                    xfrom = (int) evt.getX();
                    yfrom = (int) evt.getY();
                    game.setFromDot(xfrom, yfrom);
                    if (game.checkUndo(xfrom, yfrom))
                        counter = 0;
                    else
                        counter++;
                } else if (counter == 1) {
                    xto = (int) evt.getX();
                    yto = (int) evt.getY();
                    game.setFromDot(0, 0);
                    if (game.checkUndo(xfrom, yfrom))
                        counter = 0;
                    else {
                      //  counter = 0;
                        synchronized (t) {
                            game.play(xfrom, xto, yfrom, yto);
                        }
                        counter++;
                        if (!game.canPlay(-1)) {
                            try{
                                t.notify();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else if (counter == 2) {
                    xfrom = (int) evt.getX();
                    yfrom = (int) evt.getY();
                    game.setFromDot(xfrom, yfrom);
                    if (game.checkUndo(xfrom, yfrom))
                        counter = 0;
                    else
                        counter++;
                } else if (counter == 3) {
                    xto = (int) evt.getX();
                    yto = (int) evt.getY();
                    game.setFromDot(0, 0);
                    if (game.checkUndo(xfrom, yfrom))
                        counter = 0;
                    else {
                        counter = 0;
                        synchronized (t) {
                            game.play(xfrom, xto, yfrom, yto);
                        }
                    }
                }
            }
        }
       return true;
    }

    //Wake up thread
    public static void setValid(){
        try{
            t.notify();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Reset counter if move was invalid
    public static void takeMoveBack(){
        counter=0;
    }

    //Depending on s, play the right sound effect
    public static void playSound(int s){
        if(s == 0) {
            s0.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dice.start();
                }
            }, 300);
        }else if(s==1){
            piece.start();
        }else if(s==2){
            endW.start();
        }else if(s==3){
            endL.start();
        }
    }
}