/** Created by:
 *  Πέτρος Γκετσόπουλος 3130041
 *  Κωνσταντίνος Ευθυμίου 3130060 */
package com.miners.tavligame;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;
import java.util.Random;

public class GameView extends View{

    private final int DARK = -1;
    private final int LIGHT = 1;
    private int canvasHeight ;
    private int canvasWidth ;
    public static Board b ;
    public static Board backup ;
    int piecesHorizontal [] ;
    int piecesVertical [];
    int pieceDimens;
    int margin;

    int fromDot;
    Rect bgRect;
    boolean diceG = true;
    boolean hasPlayed = false;
    Player dark = new Player();
    Player light = new Player();

    final BitmapFactory.Options options = new BitmapFactory.Options();
    Bitmap board;

    private Paint background ;
    private Paint checkers ;
    private Paint score ;

    //Redraw the canvas on message
    Handler handler = new Handler(){
        public void handleMessage(Message msg){
            switch (msg.what){
                case 0 :
                    invalidate();break ;
                default :
                    break ;
            }
            super.handleMessage(msg);
        }
    };

    public GameView(Context context,int maxHeight , int maxWidth) {
        super(context);
        canvasHeight = maxHeight;
        canvasWidth = maxWidth;
        int left = (int)((maxWidth*10.7)/100);//Leftmost point of the board
        int right = (int)((maxWidth*92.61)/100);//Rightmost one
        margin = (int)((maxHeight*3.99)/100);//The top and bottom margins
        //if(canvasWidth/canvasHeight >1.61)
       //     pieceDimens = (int) ((maxWidth*5.31)/100);//The dimensions of each piece
      //  else pieceDimens = (int) ((maxWidth*5.71)/100);//The dimensions of each piece
        pieceDimens = ((maxHeight-(margin*3))/10);//The dimensions of each piece
        int pyramid = (int) ((maxWidth*6.457)/100);//The width dimension of each pyramid
        if(pieceDimens>pyramid) pieceDimens=pyramid;//So that it doesn't grow bigger than the actual pyramid
        bgRect = new Rect(0,0,canvasWidth,canvasHeight);
        checkers = new Paint();
        score = new Paint();
        int MY_DIP_VALUE = 20; //20dp
        int pixel= (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                MY_DIP_VALUE, getResources().getDisplayMetrics());
        score.setTextSize(pixel);
        background = new Paint();
        b = new Board();
        this.setWillNotDraw(false);
        invalidate();
        piecesHorizontal = new int[13];
        piecesVertical = new int[14];

        //The positions of the pieces in the left board
        for (int i = 0 ; i<6 ; i++ ){
            piecesVertical[i] = (left+pyramid/2+pyramid*i);
        }
        //Of the right board
        for (int i = 0 ; i<6 ; i++ ){
            piecesVertical[12-i] = (right-pyramid/2-pyramid*i);
        }

        piecesVertical[6] = (piecesVertical[5]+piecesVertical[7])/2;//The eaten pieces
        piecesVertical[13] = piecesVertical[0]/2-pyramid/4;//The pieces out of the game

        //The place of all pieces on board on Y-axis
        //6&7 to be used for the eaten pieces
        piecesHorizontal[6] = ((maxHeight/2)-pieceDimens);
        piecesHorizontal[7] = ((maxHeight/2));

        //The positions for the pieces on the top board
        for (int i = 0 ; i<5 ; i++ ){
            piecesHorizontal[i] = ((pieceDimens*i)+margin);
        }

        //On the bottom board
        for (int i = 0 ; i <5 ;i++){
            piecesHorizontal[12-i] = ( maxHeight - (pieceDimens*i)-margin);
        }
    }

    protected void onDraw(Canvas canvas ){
        super.onDraw(canvas);
        //options.inSampleSize = 1;
        board = BitmapFactory.decodeResource(getResources(), R.drawable.bg, options);
        canvas.drawBitmap(board, null, bgRect, background);

        canvas.save();
        drawBoard(canvas, b);
        canvas.restore();



    }

    public void setPlayers(Player dark,Player light){
        this.dark = dark;
        this.light = light;
    }

    //The function that plays for the AI player
    public void play(){

        int dice1,dice2;
        switch(b.getNextPlayer()) {
            case 1:
                dice1 = this.light.getDice1();
                dice2 = this.light.getDice2();
                Log.d("GV", "Light Dice1: "+dice1+" Dice2: "+dice2);
                b = new Board(light.Minimax(b, dice1, dice2));
                b.setNextPlayer(-1);
                diceG=true;
                break;
            default: break;
        }
        handler.sendMessage(Message.obtain(handler, 0));
    }

    int dicePlayed = 0;
    boolean fD = false;
    boolean fD2 = false;
    boolean sD = false;
    boolean sD2 = false;
    boolean showUndo = true;

    //The function called when the human player plays
    public void play(int x1, int x2, int y1, int y2){

        int tP = dark.getDice1()==dark.getDice2() ? 4 : 2;
        playHuman(x1,x2,y1,y2);
        if(dicePlayed==tP) {
            dicePlayed=0;
            b.setNextPlayer(1);
            diceG=true;
            fD=false;
            fD2=false;
            sD=false;
            sD2=false;
            hasPlayed=true;
            GameActivity.setValid();
        }
        handler.sendMessage(Message.obtain(handler, 0));
    }

    //Checks if the player can play with the dice he rolled
    public boolean canPlay(int player){
        if(player==-1) {
            if (b.getPieces()[25].getNumber() > 0) {
                for (int i = 1; i < 7; i++) {
                    if ((i == dark.getDice1()&&!fD) || (i == dark.getDice2()&&!sD)) {
                        if (!(b.getPieces()[25 - i].getNumber() > 1 && b.getPieces()[25 - i].getPlayer() == LIGHT))
                            return true;
                    }
                }
                b.setNextPlayer(1);
                fD=false;
                sD=false;
                return false;
            }
            int d1, d2;
            d1 = !fD ? dark.getDice1() : 0;
            d2 = !sD ? dark.getDice2() : 0;
            if(d1==0)
                d1=d2;
            if(d2==0)
                d2=d1;
            ArrayList<Move> move = b.possibleMoves(d1, d2, -1);
            if(move.size()==0 && hasPlayed&&(!sD||!fD)) {
                Log.d("GAM2", d1+" "+d2+" "+move.size());
                b.setNextPlayer(1);
                fD=false;
                sD=false;
                return false;
            }
            else return true;
        }
        else {
            if (b.getPieces()[0].getNumber() > 0) {
                for (int i = 1; i < 7; i++) {
                    if (i == light.getDice1() || i == light.getDice2()) {
                        if (!(b.getPieces()[i].getNumber() > 1 && b.getPieces()[i].getPlayer() == DARK))
                            return true;
                    }
                }
                diceG=true;
                return false;
            }
            ArrayList<Move> move = b.possibleMoves(light.getDice1(), light.getDice2(), 1);
            if(move.size()==0 && hasPlayed)
                return false;
            else return true;
        }
    }

    //Playes the moves the human player did
    public void playHuman(int x1, int x2, int y1, int y2) {
        int from = closerXY(x1, y1, 0);
        int to = closerXY(x2, y2, 0);
        if (b.checkMove(from, to, -1, dark.getDice1(), dark.getDice2())) {
            if ((from - to == dark.getDice1() && !fD)||(to==27&&from == dark.getDice1()&& !fD)) {
                if(dark.getDice1() == dark.getDice2()) {
                    if (fD2)
                        fD = true;
                    else
                        fD2=true;
                }
                else fD=true;
                backup = new Board(b);
                b.piecesOnBoard(from, to, -1);
                GameActivity.playSound(1);
                dicePlayed++;
                showUndo = true;
            } else if ((from - to == dark.getDice2() && !sD)||(to==27&&from == dark.getDice2()&& !sD)) {
                if(dark.getDice1() == dark.getDice2()) {
                    if (sD2)
                        sD = true;
                    else
                        sD2=true;
                }
                else sD=true;
                backup = new Board(b);
                b.piecesOnBoard(from, to, -1);
                GameActivity.playSound(1);
                dicePlayed++;
                showUndo = true;
            } else if((from-to == (dark.getDice2()+dark.getDice1())|| (to==27 && from == (dark.getDice2()+dark.getDice1()))) &&((!sD && !fD)||(dark.getDice1()==dark.getDice2() && !sD2&&!sD))){
                if(dark.getDice1() == dark.getDice2()) {
                    if (fD2) {
                        if (fD) {
                            sD = true;
                            sD2 = true;
                        } else {
                            fD = true;
                            sD2 = true;
                        }
                    }
                    else {
                        fD2 = true;
                        fD = true;
                    }
                }
                else{
                    fD=true;
                    sD=true;
                }
                backup = new Board(b);
                b.piecesOnBoard(from, to, -1);
                GameActivity.playSound(1);
                dicePlayed+=2;
                showUndo = true;
                GameActivity.setValid();
            } else if(to==27){
                boolean cG = true;
                if(dark.getDice1() != dark.getDice2()) {
                    if (dark.getDice1() > dark.getDice2() && !fD) {
                        fD = true;
                    } else if(dark.getDice1() > dark.getDice2() && !sD && b.gatherPiece(from, to, -1, dark.getDice2(), dark.getDice2()))
                        sD=true;
                    else if (dark.getDice1() < dark.getDice2() && !sD) {
                        sD = true;
                    } else if(dark.getDice1() < dark.getDice2() && !fD && b.gatherPiece(from, to, -1, dark.getDice1(), dark.getDice1()))
                        fD = true;
                    else cG = false;

                }
                else {
                    if (fD2) {
                        if (fD) {
                            if (sD2) {
                                sD = true;
                            } else sD2 = true;
                        } else {
                            fD = true;
                        }
                    } else fD2 = true;
                }
                if(cG) {
                    dicePlayed++;
                    showUndo = true;
                    backup = new Board(b);
                    b.piecesOnBoard(from, to, -1);
                    GameActivity.playSound(1);
                }else GameActivity.takeMoveBack();
            }
            else GameActivity.takeMoveBack();
        }
    }

    //Checks if the player pressed the "UNDO" button
    public boolean checkUndo(int x, int y){
        int from = closerXY(x, y, 1);
        if(from==-99&&showUndo){
            undo();
            showUndo = false;
            return true;
        }
        return false;
    }

    //Returns the closest position of the array the player touched
    private int closerXY(int x, int y, int undo){
        int minX = Integer.MAX_VALUE;
        int minY = Integer.MAX_VALUE;
        int closerX = Integer.MAX_VALUE;
        int closerY = Integer.MAX_VALUE;
        for(int i =0; i<piecesVertical.length; i++){
            if(Math.abs(x-piecesVertical[i])<minX){
                closerX=i;
                minX = (int) Math.abs(x-piecesVertical[i]);
            }
        }
        for(int i =0; i<piecesHorizontal.length; i++){
            if(Math.abs(y-piecesHorizontal[i])<minY){
                closerY=i;
                minY = (int) Math.abs(y-piecesHorizontal[i]);
            }
        }
        int from=0;
        if(closerY>6) {
            if (closerX < 6) {
                from = closerX + 1;
            } else if (closerX > 6 && closerX < 13) {
                from = closerX;
            }
            else if(closerX==6)
                from = 25;
            else if(closerX==13) {
                if(closerY<=7&&undo ==1)
                    from = -99;
                else
                    from = 27;
            }
        }else{
            if (closerX < 6) {
                switch(closerX) {
                    case 5:
                        from = 19;
                        break;
                    case 4:
                        from = 20;
                        break;
                    case 3:
                        from = 21;
                        break;
                    case 2:
                        from = 22;
                        break;
                    case 1:
                        from = 23;
                        break;
                    case 0:
                        from = 24;
                        break;
                    default:
                        from = -1;
                        break;
                }
            } else if (closerX > 6 && closerX < 13) {
                switch(closerX) {
                    case 7:
                        from = 18;
                        break;
                    case 8:
                        from = 17;
                        break;
                    case 9:
                        from = 16;
                        break;
                    case 10:
                        from = 15;
                        break;
                    case 11:
                        from = 14;
                        break;
                    case 12:
                        from = 13;
                        break;
                    default:
                        from = -1;
                        break;
                }
            } else if(closerX==13) {
                if (closerY == 6 && undo == 1)
                    from = -99;
            }

        }
        return from;
    }

    //Shows the blue dot
    public void setFromDot(int x, int y) {
        if(x!=0 && y!=0)
            this.fromDot = closerXY(x,y, 0);
        else fromDot = 0;
        handler.sendMessage(Message.obtain(handler, 0));

    }

    //The undo function, returns to original position
    public void undo (){
        boolean pTwo=false;
        if(b.getPlayedMove().from - b.getPlayedMove().to == 2*dark.getDice1() && dark.getDice1()==dark.getDice2())
            pTwo=true;
        if(dicePlayed!=0) {
            b = backup;
            dicePlayed--;
            if (sD)
                sD = false;
            else if (sD2) {
                sD2 = false;
            } else if (fD) {
                fD = false;
            } else if (fD2) {
                fD2 = false;
            }
            if(pTwo){
                dicePlayed--;
                if (sD)
                    sD = false;
                else if (sD2) {
                    sD2 = false;
                } else if (fD) {
                    fD = false;
                } else if (fD2) {
                    fD2 = false;
                }
            }
            GameActivity.takeMoveBack();
        }
    }

    //Pretty much everything showing up
    public void drawBoard(Canvas canvas,Board board){

        options.inSampleSize = 2;
        board = this.b;
        Pieces pcs[] = new Pieces[28];
        Bitmap checker_color ;
        Bitmap undo = BitmapFactory.decodeResource(getResources(), R.drawable.undo2, options);
        Bitmap diceF ;
        Bitmap diceS ;
        pcs = board.getPieces();

        int dice1,dice2;
        int diceDimens = pieceDimens/3;
        int id;
        int toF, toS, toA;
        toF = fromDot-dark.getDice1();
        toS = fromDot-dark.getDice2();
        toA = fromDot-dark.getDice2()-dark.getDice1();
        Bitmap blueDot = BitmapFactory.decodeResource(getResources(), R.drawable.blue, options);
        Bitmap greenDot = BitmapFactory.decodeResource(getResources(), R.drawable.green, options);

        /* Drawing of the pieces on Board
         * Draws all pieces, including eaten
         * and gathered pieces
         * Also draws the numbers of eaten/gathered
         * pieces and pieces over 5*/
        for (int i = 1 ; i <25 ; i++){
            if(pcs[i].getPlayer()!=0){
                if(pcs[i].getPlayer()==DARK){
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    score.setColor(Color.WHITE);
                    checker_color = BitmapFactory.decodeResource(getResources(), R.drawable.dark, options);
                }
                else
                {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                  //  options.inSampleSize = 2;
                    score.setColor(Color.BLACK);
                    checker_color = BitmapFactory.decodeResource(getResources(), R.drawable.light, options);
                }
                int numOfPieces = pcs[i].getNumber();
                boolean outnumbered = false ;
                int extra = 0; //For when we have more than 5 pieces
                if(numOfPieces>5){
                    outnumbered = true ;
                    extra = numOfPieces - 5 ;
                    numOfPieces = 5 ;
                }
                if(i<7){
                    for(int j= numOfPieces-1 ; j >=0;j--){
                        int up = 12-j ;
                        canvas.save();
                        canvas.drawBitmap(checker_color, null, new Rect(piecesVertical[i - 1] - pieceDimens / 2, piecesHorizontal[up] - pieceDimens, piecesVertical[i - 1] + pieceDimens / 2, piecesHorizontal[up]), checkers);
                        canvas.restore();
                    }
                    if(outnumbered){
                        canvas.save();
                        canvas.drawText("" + (extra+5), piecesVertical[i-1]-pieceDimens/6, piecesHorizontal[12]-pieceDimens/3, score);
                        canvas.restore();
                    }
                }
                else if (i<13){
                    for(int j = numOfPieces - 1 ; j >= 0 ; j--){
                        int up = 12-j ;
                        canvas.save();
                        canvas.drawBitmap(checker_color, null,new Rect(piecesVertical[i]-pieceDimens/2,piecesHorizontal[up]-pieceDimens,piecesVertical[i]+pieceDimens/2,piecesHorizontal[up]),checkers);
                         canvas.restore();
                    }
                    if(outnumbered){
                        canvas.save();
                        canvas.drawText(""+(extra+5), piecesVertical[i]-pieceDimens/6, piecesHorizontal[12]-pieceDimens/3, score);
                        canvas.restore();
                    }
                }

                else if(i<19){
                    int offset ;
                    switch (i){
                        case 13: offset=12; break;
                        case 14: offset=11; break;
                        case 15: offset=10; break;
                        case 16: offset=9; break;
                        case 17: offset=8; break;
                        case 18: offset=7; break;
                        default:offset=-1; break;
                    }
                    for(int j=0 ; j<numOfPieces ; j++){
                        canvas.save();
                        canvas.drawBitmap(checker_color, null,new Rect(piecesVertical[offset]-pieceDimens/2,piecesHorizontal[j],piecesVertical[offset]+pieceDimens/2,piecesHorizontal[j]+pieceDimens),checkers);
                        canvas.restore();
                    }
                    if(outnumbered){
                        canvas.save();
                        canvas.drawText(""+(extra+5), piecesVertical[offset]-pieceDimens/6, piecesHorizontal[1]-pieceDimens/3, score);
                        canvas.restore();
                    }
                }
                else if(i<25){
                    int offset ;
                    switch (i){
                        case 19: offset=5; break;
                        case 20: offset=4; break;
                        case 21: offset=3; break;
                        case 22: offset=2; break;
                        case 23: offset=1; break;
                        case 24: offset=0; break;
                        default:offset=-1; break;
                    }
                    for(int j = 0 ; j<numOfPieces;j++){
                        canvas.save();
                        canvas.drawBitmap(checker_color, null,new Rect(piecesVertical[offset]-pieceDimens/2,piecesHorizontal[j],piecesVertical[offset]+pieceDimens/2,piecesHorizontal[j]+pieceDimens),checkers);
                        canvas.restore();
                    }
                    if(outnumbered){
                        canvas.save();
                        canvas.drawText("" + (extra+5), piecesVertical[offset]-pieceDimens/6, piecesHorizontal[1]-pieceDimens/3, score);
                        canvas.restore();
                    }
                }
            }
            /* Drawing of the little dots, so
             * that we know what we chose
             * and what we can play
             * Shows both dices AND the full dice*/
            if(i<7){
                if(fromDot==i)
                    canvas.drawBitmap(blueDot, null, new Rect(piecesVertical[i - 1] - margin / 2, piecesHorizontal[12], piecesVertical[i - 1] + margin / 2, piecesHorizontal[12] + margin), checkers);
                if(toF==i&&!fD)
                    if (b.checkMove(fromDot, toF, -1, dark.getDice1(), dark.getDice1()))
                        canvas.drawBitmap(greenDot, null, new Rect(piecesVertical[i - 1] - margin / 2, piecesHorizontal[12], piecesVertical[i - 1] + margin / 2, piecesHorizontal[12] + margin), checkers);
                if(toS==i&&!sD)
                    if (b.checkMove(fromDot, toS, -1, dark.getDice2(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null, new Rect(piecesVertical[i - 1] - margin / 2, piecesHorizontal[12], piecesVertical[i - 1] + margin / 2, piecesHorizontal[12] + margin), checkers);
                if((toA==i&&!sD&&!fD)||(toA==i&& dark.getDice1()== dark.getDice2() && !sD && !sD2))
                    if (b.checkMove(fromDot, toA, -1, dark.getDice1(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null, new Rect(piecesVertical[i - 1] - margin / 2, piecesHorizontal[12], piecesVertical[i - 1] + margin / 2, piecesHorizontal[12] + margin), checkers);
            } else if (i<13){
                if(fromDot==i)
                    canvas.drawBitmap(blueDot, null, new Rect(piecesVertical[i] - margin / 2, piecesHorizontal[12], piecesVertical[i] + margin / 2, piecesHorizontal[12]+margin), checkers);
                if(toF==i&&!fD)
                    if(b.checkMove(fromDot, toF, -1, dark.getDice1(), dark.getDice1()))
                        canvas.drawBitmap(greenDot, null, new Rect(piecesVertical[i] - margin / 2, piecesHorizontal[12], piecesVertical[i] + margin / 2, piecesHorizontal[12]+margin), checkers);
                if(toS==i&&!sD)
                    if(b.checkMove(fromDot, toS, -1, dark.getDice2(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null, new Rect(piecesVertical[i] - margin / 2, piecesHorizontal[12], piecesVertical[i] + margin / 2, piecesHorizontal[12]+margin), checkers);
                if((toA==i&&!sD&&!fD)||(toA==i&& dark.getDice1()== dark.getDice2() && !sD && !sD2))
                    if (b.checkMove(fromDot, toA, -1, dark.getDice1(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null, new Rect(piecesVertical[i] - margin / 2, piecesHorizontal[12], piecesVertical[i] + margin / 2, piecesHorizontal[12]+margin), checkers);
            }else if(i<19) {
                int offset;
                switch (i) {
                    case 13:
                        offset = 12;
                        break;
                    case 14:
                        offset = 11;
                        break;
                    case 15:
                        offset = 10;
                        break;
                    case 16:
                        offset = 9;
                        break;
                    case 17:
                        offset = 8;
                        break;
                    case 18:
                        offset = 7;
                        break;
                    default:
                        offset = -1;
                        break;
                }
                if(fromDot==i)
                    canvas.drawBitmap(blueDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
                if(toF==i&&!fD)
                    if(b.checkMove(fromDot, toF, -1, dark.getDice1(), dark.getDice1()))
                        canvas.drawBitmap(greenDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
                if(toS==i&&!sD)
                    if(b.checkMove(fromDot, toS, -1, dark.getDice2(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
                if((toA==i&&!sD&&!fD)||(toA==i&& dark.getDice1()== dark.getDice2() && !sD && !sD2))
                    if (b.checkMove(fromDot, toA, -1, dark.getDice1(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
            } else if(i<25) {
                int offset;
                switch (i) {
                    case 19:
                        offset = 5;
                        break;
                    case 20:
                        offset = 4;
                        break;
                    case 21:
                        offset = 3;
                        break;
                    case 22:
                        offset = 2;
                        break;
                    case 23:
                        offset = 1;
                        break;
                    case 24:
                        offset = 0;
                        break;
                    default:
                        offset = -1;
                        break;
                }
                if(fromDot==i)
                    canvas.drawBitmap(blueDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
                if(toF==i&&!fD)
                    if(b.checkMove(fromDot, toF, -1, dark.getDice1(), dark.getDice1()))
                        canvas.drawBitmap(greenDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
                if(toS==i&&!sD)
                    if(b.checkMove(fromDot, toS, -1, dark.getDice2(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
                if((toA==i&&!sD&&!fD)||(toA==i&& dark.getDice1()== dark.getDice2() && !sD && !sD2))
                    if (b.checkMove(fromDot, toA, -1, dark.getDice1(), dark.getDice2()))
                        canvas.drawBitmap(greenDot, null,new Rect(piecesVertical[offset]-margin/2,piecesHorizontal[0]-margin,piecesVertical[offset]+margin/2,piecesHorizontal[0]),checkers);
            }
        }

		//Displays eaten pieces for Light player
        if(pcs[0].getNumber()>0) {
            score.setColor(Color.BLACK);
            checker_color = BitmapFactory.decodeResource(getResources(), R.drawable.light);
            canvas.drawBitmap(checker_color, null,new Rect(piecesVertical[6]-pieceDimens/2,piecesHorizontal[6],piecesVertical[6]+pieceDimens/2,piecesHorizontal[6]+pieceDimens),checkers);
            canvas.drawText(Integer.toString(pcs[0].getNumber()), piecesVertical[6]-pieceDimens/6,piecesHorizontal[7]-pieceDimens/3, score);
        }

		//Dark player's eaten pieces
        if(pcs[25].getNumber()>0) {
            score.setColor(Color.WHITE);
            checker_color = BitmapFactory.decodeResource(getResources(), R.drawable.dark);
            canvas.drawBitmap(checker_color, null,new Rect(piecesVertical[6]-pieceDimens/2,piecesHorizontal[7],piecesVertical[6]+pieceDimens/2,piecesHorizontal[7]+pieceDimens),checkers);
            canvas.drawText(Integer.toString(pcs[25].getNumber()), piecesVertical[6]-pieceDimens/7,piecesHorizontal[8]-pieceDimens/3, score);
        }

        if(pcs[26].getNumber()>0) {
            score.setColor(Color.BLACK);
            checker_color = BitmapFactory.decodeResource(getResources(), R.drawable.light);
            canvas.drawBitmap(checker_color, null, new Rect(piecesVertical[13] - pieceDimens / 2, piecesHorizontal[0], piecesVertical[13] + pieceDimens / 2, piecesHorizontal[0] + pieceDimens), checkers);
            canvas.drawText(Integer.toString(pcs[26].getNumber()), piecesVertical[13]-pieceDimens/5,piecesHorizontal[1]-pieceDimens/3, score);
        }

        if(pcs[27].getNumber()>0) {
            score.setColor(Color.WHITE);
            checker_color = BitmapFactory.decodeResource(getResources(), R.drawable.dark);
            canvas.drawBitmap(checker_color, null, new Rect(piecesVertical[13] - pieceDimens / 2, piecesHorizontal[11], piecesVertical[13] + pieceDimens / 2, piecesHorizontal[11] + pieceDimens), checkers);
            canvas.drawText(Integer.toString(pcs[27].getNumber()), piecesVertical[13]-pieceDimens/6,piecesHorizontal[12]-pieceDimens/3, score);
        }

        if(fromDot==25)
            canvas.drawBitmap(blueDot, null,new Rect(piecesVertical[6]-margin/2,piecesHorizontal[7]+pieceDimens,piecesVertical[6]+margin/2,piecesHorizontal[7]+pieceDimens+margin),checkers);
        if((toF<1&&!fD)||(toS<1&&!sD)||(toA<1&&!fD&&!sD))
            if(b.checkMove(fromDot, 27, -1, dark.getDice1(), dark.getDice2()))
                canvas.drawBitmap(greenDot, null,new Rect(piecesVertical[13]-margin/2,piecesHorizontal[12],piecesVertical[13]+margin/2,piecesHorizontal[12]+margin),checkers);



        switch(board.getNextPlayer()) {
            case -1 ://Dark
                if(diceG) {
                    dark.rollDices();
                    diceG=false;
                }
                String shouldGrey = "";
                if(fD2) shouldGrey="pp";
                if(fD) shouldGrey="p";
                dice1 = dark.getDice1();
                id = getResources().getIdentifier("dice" + dice1 + shouldGrey, "drawable", getContext().getPackageName());
                diceF = BitmapFactory.decodeResource(getResources(), id, options);
                dice2 = dark.getDice2();
                canvas.drawBitmap(diceF, null,new Rect(piecesVertical[2]-diceDimens,piecesHorizontal[7]-diceDimens,piecesVertical[2]+diceDimens,piecesHorizontal[7]+diceDimens),checkers);
                shouldGrey="";
                if(sD2) shouldGrey = "pp";
                if(sD) shouldGrey = "p";
                id = getResources().getIdentifier("dice" + dice2 + shouldGrey, "drawable", getContext().getPackageName());
                diceS = BitmapFactory.decodeResource(getResources(), id, options);
                canvas.drawBitmap(diceS, null, new Rect(piecesVertical[3] - diceDimens, piecesHorizontal[7] - diceDimens, piecesVertical[3] + diceDimens, piecesHorizontal[7] + diceDimens), checkers);
                if((sD||fD||sD2||fD2)&& showUndo)
                    canvas.drawBitmap(undo, null, new Rect(0, piecesHorizontal[6], piecesVertical[0]-pieceDimens/2-pieceDimens/6 , piecesHorizontal[7]), checkers);
                score.setColor(Color.WHITE);
                if(!b.gameOver())
                    if(canPlay(-1))
                        canvas.drawText("Your turn", piecesVertical[9]-pieceDimens/2, piecesHorizontal[7], score);
                    else {
                        canvas.drawText("You can't play!", piecesVertical[9]-pieceDimens/2, piecesHorizontal[7], score);

                    }
                break;

            case 1 :
                if(diceG) {
                    light.rollDices();
                    diceG=false;
                }
                dice1 = light.getDice1();
                dice2 = light.getDice2();

                id = getResources().getIdentifier("dice" + dice1, "drawable", getContext().getPackageName());
                diceF = BitmapFactory.decodeResource(getResources(), id, options);
                canvas.drawBitmap(diceF, null,new Rect(piecesVertical[2]-diceDimens,piecesHorizontal[7]-diceDimens,piecesVertical[2]+diceDimens,piecesHorizontal[7]+diceDimens),checkers);
                id = getResources().getIdentifier("dice" + dice2, "drawable", getContext().getPackageName());
                diceS = BitmapFactory.decodeResource(getResources(), id, options);
                canvas.drawBitmap(diceS, null, new Rect(piecesVertical[3] - diceDimens, piecesHorizontal[7] - diceDimens, piecesVertical[3] + diceDimens, piecesHorizontal[7] + diceDimens), checkers);
                score.setColor(Color.BLACK);
                if(!b.gameOver())
                    if(canPlay(1))
                       canvas.drawText("AI's turn", piecesVertical[9]-pieceDimens/2, piecesHorizontal[7], score);
                    else {
                        float tempSize = score.getTextSize();
                        score.setTextSize(score.getTextSize()*8/9);
                        canvas.drawText("AI can't play!", piecesVertical[9] - pieceDimens, piecesHorizontal[7]-score.getTextSize()/2, score);
                        canvas.drawText("Touch to continue", piecesVertical[9] - pieceDimens*3/2, piecesHorizontal[7] + score.getTextSize()/2, score);
                        score.setTextSize(tempSize);
                    }
                break;
            default: break;
        }
        if(b.getPieces()[27].getNumber()==15){
            canvas.drawText("Wow! You won!", piecesVertical[9]-pieceDimens, piecesHorizontal[7], score);
            GameActivity.playSound(2);
        }else if(b.getPieces()[26].getNumber()==15) {
            canvas.drawText("You lost!", piecesVertical[9] - pieceDimens / 2, piecesHorizontal[7], score);
            GameActivity.playSound(3);
        }


    }
}
