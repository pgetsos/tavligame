/** Created by:
 *  Πέτρος Γκετσόπουλος 3130041
 *  Κωνσταντίνος Ευθυμίου 3130060 */
package com.miners.tavligame;

import android.media.MediaPlayer;
import android.util.Log;

import java.util.Random;
import java.util.ArrayList;

public class Player {

    private int dice1 = 0;
    private int dice2 = 0;
    private Board bestMinMove;
    private Board bestMaxMove;
    private final int LIGHT = 1;
    private int Maxdepth = 3;

    public Player(){

    }

    public Player(int depth){
        this.Maxdepth = depth;
    }

    //Roll the dices, play rolling dice sound effect
    public void rollDices(){
        GameActivity.playSound(0);
        Random r = new Random();
        this.dice1 = r.nextInt(6)+1;
        this.dice2 = r.nextInt(6)+1;
    }

    public int getDice2() {
        return dice2;
    }

    public int getDice1() {
        return dice1;
    }

    //Returns the board after the best move for the player
    public Board Minimax(Board b, int ddice1, int ddice2) {
        Board tempBoard = new Board(b);
        if (tempBoard.getNextPlayer() == LIGHT) {
            tempBoard = min(new Board(b), ddice1, ddice2, 0);//mpainei edo
        }
        else{
            tempBoard = max(new Board(b), ddice1, ddice2, 0);
        }
        return tempBoard;
    }

    // Returns the best move for LIGHT player
    public Board min(Board b, int ddice1, int ddice2, int depth) {
        if (b.gameOver()||depth == Maxdepth) {
            return b;
        } else {
            bestMinMove = new Board(b);
            ArrayList<Board> moves = b.possibleStates(ddice1, ddice2, 1);
            int min = Integer.MAX_VALUE;
            for (Board move : moves) {
                int d1 = ddice1;
                int d2 = ddice1!=ddice2 ? ddice2 : 7-ddice2;
                Board bMove = max(move, d1, d2, depth + 1);
                if (bMove.evaluate() < min) {
                    min = bMove.evaluate();
                    bestMinMove = new Board(move);
                }
            }
            return bestMinMove;
        }
    }

    //Returns the best move for DARK player
    public Board max(Board b, int ddice1, int ddice2, int depth) {
        if (b.gameOver()||depth == Maxdepth){
            return b;
        }
        else {
            bestMaxMove = new Board(b);
            ArrayList<Board> moves = bestMaxMove.possibleStates(ddice1, ddice2, -1);
            int max = Integer.MIN_VALUE;
            for (Board move : moves) {
                int d1 = ddice1;
                int d2 = ddice1!=ddice2 ? ddice2 : 7-ddice2;
                Board xMove = min(move, d1, d2, depth + 1);
                if (xMove.evaluate() > max) {
                    max = xMove.evaluate();
                    bestMaxMove = new Board(move);
                }
            }
            return bestMaxMove;
        }
    }
}