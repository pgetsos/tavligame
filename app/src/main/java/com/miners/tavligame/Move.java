/** Created by:
 *  Πέτρος Γκετσόπουλος 3130041
 *  Κωνσταντίνος Ευθυμίου 3130060 */
package com.miners.tavligame;

//Class for Move-type objects
public class Move {

    int from, to, player;

    public Move(){
        this.from = 0;
        this.to = 0;
        this.player = 0;
    }

    public Move(int from, int to, int player) {
        this.from = from;
        this.to = to;
        this.player = player;
    }

    public Move(Move move){
        this.from = move.from;
        this.to = move.to;
        this.player = move.player;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public int getPlayer() {
        return player;
    }

    public void setPlayer(int player) {
        this.player = player;
    }
}
