/** Created by:
 *  Πέτρος Γκετσόπουλος 3130041
 *  Κωνσταντίνος Ευθυμίου 3130060 */
package com.miners.tavligame;

import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Board {

    private Pieces[] pieces;
    private Move playedMove;

    private final int DARK = -1;
    private final int LIGHT = 1;
    private int nextPlayer = DARK;

    public Board() {

        //Initialize board
        pieces = new Pieces[28];
        playedMove = new Move();

        for(int i=0; i<28; i++) {
            pieces[i] = new Pieces();
        }

        pieces[1].setPlayer(1);
        pieces[1].setNumber(2);
        pieces[6].setPlayer(-1);
        pieces[6].setNumber(5);
        pieces[8].setPlayer(-1);
        pieces[8].setNumber(3);
        pieces[12].setPlayer(1);
        pieces[12].setNumber(5);
        pieces[13].setPlayer(-1);
        pieces[13].setNumber(5);
        pieces[17].setPlayer(1);
        pieces[17].setNumber(3);
        pieces[19].setPlayer(1);
        pieces[19].setNumber(5);
        pieces[24].setPlayer(-1);
        pieces[24].setNumber(2);

        /* 0 for Light Player's eaten pills
         * 25 for Dark's
         * 26 for Light's collected
         * 27 for Dark's */

        pieces[0].setPlayer(1);
        pieces[25].setPlayer(-1);
        pieces[26].setPlayer(1);
        pieces[27].setPlayer(-1);
    }

    public Board(Board board) {

        this.nextPlayer = board.nextPlayer;
        this.playedMove = board.playedMove;
        this.pieces = new Pieces[28];
        for(int i=0; i<28; i++) {
            pieces[i] = new Pieces(board.pieces[i]);
        }
    }

    public Pieces[] getPieces() {
        return pieces;
    }

    public void setPieces(Pieces[] pieces) {
        for(int i=0; i<28; i++) {
            this.pieces[i] = new Pieces(pieces[i]);
        }
    }

    public Move getPlayedMove() {
        return playedMove;
    }

    public void setPlayedMove(Move playedMove) {
        this.playedMove = playedMove;
    }

    public int getNextPlayer() {
        return nextPlayer;
    }

    public void setNextPlayer(int nextPlayer) {
        this.nextPlayer = nextPlayer;
    }

    //Change the placement of pieces on the array of the board
    public void piecesOnBoard(int from, int to, int player) {
        if(pieces[from].getPlayer()!=pieces[to].getPlayer() && pieces[to].getPlayer()!=0){
            if(pieces[to].getPlayer()==DARK)
                pieces[25].addPiece();
            else {
                pieces[0].addPiece();
            }
            pieces[to].removePiece();
        }

        //Remove piece and reset Player if number of pieces == 0
        pieces[from].removePiece();
        if(pieces[from].getNumber()==0 && from >=1 && from <=24)
            pieces[from].setPlayer(0);
        pieces[to].setPlayer(player);
        pieces[to].addPiece();
        //nextPlayer*=-1;
        playedMove = new Move(from, to, player);
    }

    //Checking if a player can start gathering his pieces
    public boolean gathering (int player){
        int min=0;
        int max=25;
        if(player==DARK) {
            min = 7;
        }
        else max = 18;
        for(int i = min; i<=max; i++){
            if(pieces[i].getPlayer()==player && pieces[i].getNumber()>0)
                return false;
        }
        return true;
    }

    //Check if the player moves an eaten piece
    public boolean moveEaten(int from, int player){
        if(player==LIGHT){
            if(from!=0 && pieces[0].getNumber()>0)
                return false;
        }
        else if(player==DARK) {
            if(from!=25 && pieces[25].getNumber()>0)
                return false;
        }
        return true;
    }

    //Check if a piece can be gathered
    public boolean gatherPiece(int from, int to, int player, int dice1, int dice2){
        if(!gathering(player)) return false;
        if(player==LIGHT){
            if(to==26){
                int lastPiece=19;
                for(int i=19; i<=24; i++){
                    if(pieces[i].getNumber()>0&&pieces[i].getPlayer()==LIGHT) {
                        lastPiece = i;
                        break;
                    }
                }
                if((from!=lastPiece && (25-from)!=dice1 && (25-from)!=dice2) || from==lastPiece && (25-lastPiece)>Math.max(dice1, dice2)){
                    return false;
                }
            }
        }
        if(player==DARK){
            if(to==27){
                int lastPiece=6;
                for(int i=6; i>=1; i--){
                    if(pieces[i].getNumber()>0&&pieces[i].getPlayer()==DARK) {
                        lastPiece = i;
                        break;
                    }                }
                if((from!=lastPiece && from!=dice1 && from!=dice2) || from==lastPiece && (lastPiece)>Math.max(dice1, dice2)|| lastPiece>dice1+dice2 && from!=dice1 && from!=dice2){
                    return false;
                }
            }
        }
        return true;
    }

    //Check if the move is doable or not
    public boolean checkMove(int from, int to, int player, int dice1, int dice2){
        //Initial checks for simple illegal moves
        if(from==to || pieces[from].getPlayer()!=player || from==27 || from==26)
            return false;

        //Check if the move is to a legal position(forward-occupied-distance traveled)
        if((player==DARK && from<to && to!=27) || (player==LIGHT &&to<from))
            return false;
        if(pieces[to].getPlayer() == (player*-1) && pieces[to].getNumber()>1)
            return false;
        if((Math.abs(to-from)!=dice1) && (Math.abs(to-from)!=dice2) && (from-to)!=(dice1+dice2) && !this.gathering(player))//|| (to==27 && from > (dice1+dice2)))
            return false;
        if(from-to == dice1+dice2 && !checkMove(from, from-dice1, player, dice1, dice1) && !checkMove(from, from-dice2, player, dice2, dice2) && player==-1)
            return false;

        //Check if the piece can be moved
        if(!moveEaten(from, player))
            return false;
        if((from==25 && player==DARK && pieces[25].getNumber()==0)||(from==0 && player==LIGHT && pieces[0].getNumber()==0))
            return false;

        //Gathering checks
        if(to==26 && player==DARK || to==27 && player==LIGHT)
            return false;
        if(to==26 && !gathering(LIGHT) || to==27 && !gathering(DARK))
            return false;
        if(gathering(player) && (to==26||to==27)) { //<-----------May have problem here, have to proof-check!!!
            if (!gatherPiece(from, to, player, dice1, dice2))
                return false;
        }

        return true;
    }

    //Check if the game is over
    public boolean gameOver() {
        return (pieces[26].getNumber()==15 || pieces[27].getNumber()==15);
    }

    //Create an ArrayList of type 'Move', filled with all the possible moves the player can do
    public ArrayList<Move> possibleMoves(int dice1,int dice2, int player) {
        int counter = 0;
        ArrayList<Move> moves = new ArrayList<>();
        ArrayList<Integer> positions = new ArrayList<>();

        //Fill positions ArrayList with all the positions of pieces on board
        for(int i=0; i<26; i++) {
            if(pieces[i].getPlayer()==player && pieces[i].getNumber()>0) {
                counter++;
                positions.add(i);
            }
        }

        for(int i=0; i<counter; i++) {
            int from = positions.get(i);

            if (player == LIGHT) {
                if (from + dice1 > 24) {
                    if(checkMove(from, 26, player, dice1, dice2)) //Check move if possible
                        moves.add(new Move(from, 26, player));
                } else {
                    if(checkMove(from, from + dice1, player, dice1, dice2)) {
                        moves.add(new Move(from, from + dice1, player));
                    }
                }

                //Check if it's doubles to avoid duplicates
                if(dice1!=dice2) {
                    if (from + dice2 > 24) {
                        if(checkMove(from, 26, player, dice1, dice2))
                            moves.add(new Move(from, 26, player));
                    } else {
                        if(checkMove(from, from + dice2, player, dice1, dice2))
                            moves.add(new Move(from, from + dice2, player));
                    }
                }
            }
            else if (player == DARK) {
                if (from - dice1 < 1) {
                    if(checkMove(from, 27, player, dice1, dice2))
                        moves.add(new Move(from, 27, player));
                } else {
                    if(checkMove(from, from - dice1, player, dice1, dice2))
                        moves.add(new Move(from, from - dice1, player));
                }
                if(dice1!=dice2) {
                    if (from - dice2 < 1) {
                        if(checkMove(from, 27, player, dice1, dice2))
                            moves.add(new Move(from, 27, player));
                    } else {
                        if(checkMove(from, from - dice2, player, dice1, dice2))
                            moves.add(new Move(from, from - dice2, player));
                    }
                }
            }

        }
        return moves;
    }

    public ArrayList<Board> possibleStates(int dice1, int dice2, int player) {
        ArrayList<Board> states = new ArrayList<>();
        ArrayList<Board> finalStates = new ArrayList<>();

        if (dice1 != dice2) {
            ArrayList<Board> temp = new ArrayList<>();
            ArrayList<Move> moves = possibleMoves(dice1, dice2, player);

            //States when playing the first dice
            for (int i = 0; i < moves.size(); i++) {
                Board alt = new Board(this);
                alt.piecesOnBoard(moves.get(i).getFrom(), moves.get(i).getTo(), player);
                if (alt.gameOver()) {
                    finalStates.add(alt);//If it's final state, saved elsewhere
                } else {
                    //Log.d("Board", "From: " + alt.getPlayedMove().from + " To:" + alt.getPlayedMove().to + " Value: " + alt.evaluate());

                    temp.add(alt);//If not, save to temp array to play the second dice
                }
            }
            ArrayList<Board> temp2 = new ArrayList<>();

            //Playing the other dice
            for (int k = 0; k < temp.size(); k++) {
                Board forBoard = temp.get(k);

                //Find which dice was played in the first state
                if (Math.abs(forBoard.getPlayedMove().getFrom() - forBoard.getPlayedMove().getTo()) == dice1) {

                    //Get possible moves with the second dice only
                    ArrayList<Move> tempMoves = forBoard.possibleMoves(dice2, dice2, player);
                    for (int i = 0; i < tempMoves.size(); i++) {
                        Board tempBoard = new Board(forBoard);
                        tempBoard.piecesOnBoard(tempMoves.get(i).getFrom(), tempMoves.get(i).getTo(), player);
                        temp2.add(tempBoard);
                    }
                    if (temp2.size() > 0) {
                        for (int tD = 0; tD < temp2.size(); tD++) {
                            finalStates.add(temp2.get(tD));
                        }
                    } else {
                        finalStates.add(forBoard);
                    }
                } else if (Math.abs(forBoard.getPlayedMove().getFrom() - forBoard.getPlayedMove().getTo()) == dice2) {

                    ArrayList<Move> tempMoves = forBoard.possibleMoves(dice1, dice1, player);

                    for (int i = 0; i < tempMoves.size(); i++) {
                        Board tempBoard = new Board(forBoard);
                        tempBoard.piecesOnBoard(tempMoves.get(i).getFrom(), tempMoves.get(i).getTo(), player);
                        temp2.add(tempBoard);
                    }
                    if (temp2.size() > 0) {
                        for (int tD = 0; tD < temp2.size(); tD++) {
                            finalStates.add(temp2.get(tD));
                        }
                    } else {
                        finalStates.add(forBoard);
                    }
                } else {//For gathering purposes

                    boolean s1 = false;
                    boolean s2 = false;
                    boolean s3 = false;

                    if (player == LIGHT) {
                        s1 = ((forBoard.getPlayedMove().getFrom() + dice1) > 25 && forBoard.getPlayedMove().getFrom() + dice2 > 25);
                        s2 = (forBoard.getPlayedMove().getFrom() + dice1 == 25);
                        s3 = (forBoard.getPlayedMove().getFrom() + dice2 == 25);
                    } else if (player == DARK) {
                        s1 = ((forBoard.getPlayedMove().getFrom() - dice1) < 0 && forBoard.getPlayedMove().getFrom() - dice2 < 0);
                        s2 = (forBoard.getPlayedMove().getFrom() - dice1 == 0);
                        s3 = (forBoard.getPlayedMove().getFrom() - dice2 == 0);
                    }

                    //If both dices+froms go outside the board
                    if (s1) {
                        if (dice1 < dice2) {
                            ArrayList<Move> tempMoves = forBoard.possibleMoves(dice1, dice1, player);
                            for (int i = 0; i < tempMoves.size(); i++) {
                                Board tempBoard = new Board(forBoard);
                                tempBoard.piecesOnBoard(tempMoves.get(i).getFrom(), tempMoves.get(i).getTo(), player);
                                temp2.add(tempBoard);
                            }
                            if (temp2.size() > 0) {
                                for (int tD = 0; tD < temp2.size(); tD++) {
                                    finalStates.add(temp2.get(tD));
                                }
                            } else {
                                finalStates.add(forBoard);
                            }
                        } else {
                            ArrayList<Move> tempMoves = forBoard.possibleMoves(dice2, dice2, player);
                            for (int i = 0; i < tempMoves.size(); i++) {
                                Board tempBoard = new Board(forBoard);
                                tempBoard.piecesOnBoard(tempMoves.get(i).getFrom(), tempMoves.get(i).getTo(), player);
                                temp2.add(tempBoard);
                            }
                            if (temp2.size() > 0) {
                                for (int tD = 0; tD < temp2.size(); tD++) {
                                    finalStates.add(temp2.get(tD));
                                }
                            } else {
                                finalStates.add(forBoard);
                            }
                        }
                    } else if (s2) {//If the 1st dice+from goes just outside board

                        ArrayList<Move> tempMoves = forBoard.possibleMoves(dice2, dice2, player);
                        for (int i = 0; i < tempMoves.size(); i++) {
                            Board tempBoard = new Board(forBoard);
                            tempBoard.piecesOnBoard(tempMoves.get(i).getFrom(), tempMoves.get(i).getTo(), player);
                            temp2.add(tempBoard);
                        }
                        if (temp2.size() > 0) {
                            for (int tD = 0; tD < temp2.size(); tD++) {
                                finalStates.add(temp2.get(tD));
                            }
                        } else {
                            finalStates.add(forBoard);
                        }
                    } else if (s3) {//If the 2nd dice+from goes just outside board

                        ArrayList<Move> tempMoves = forBoard.possibleMoves(dice1, dice1, player);
                        for (int i = 0; i < tempMoves.size(); i++) {
                            Board tempBoard = new Board(forBoard);
                            tempBoard.piecesOnBoard(tempMoves.get(i).getFrom(), tempMoves.get(i).getTo(), player);
                            temp2.add(tempBoard);
                        }
                        if (temp2.size() > 0) {
                            for (int tD = 0; tD < temp2.size(); tD++) {
                                finalStates.add(temp2.get(tD));
                            }
                        } else {
                            finalStates.add(forBoard);
                        }
                    }
                }
            }
        } else {//If doubles
            ArrayList<Board> first, second, third, fourth;//4 times running the loops

            //Initialize the 4 ArrayLists
            first = new ArrayList<>();
            second = new ArrayList<>();
            third = new ArrayList<>();
            fourth = new ArrayList<>();

            //First dice, create array of possible moves, add all states in the ArrayList
            ArrayList<Move> tempMoves = possibleMoves(dice1, dice1, player);
            for (int i = 0; i < tempMoves.size(); i++) {
                Board tempBoard = new Board(this);
                tempBoard.piecesOnBoard(tempMoves.get(i).getFrom(), tempMoves.get(i).getTo(), player);
                first.add(tempBoard);
            }

            //For every state(Board) check if it's game over, then continue to next dice
            for (int i = 0; i< first.size(); i++) {
                Board forArray1 = first.get(i);
                if (forArray1.gameOver()) {
                    finalStates.add(forArray1);
                    break;
                }

                //Repeating array creation
                ArrayList<Move> tempMoves2 = forArray1.possibleMoves(dice1, dice1, player);
                for (int j = 0; j < tempMoves2.size(); j++) {
                    Board tempBoard = new Board(forArray1);
                    tempBoard.piecesOnBoard(tempMoves2.get(j).getFrom(), tempMoves2.get(j).getTo(), player);
                    second.add(tempBoard);
                }

                /* If there are no more possible moves to be played,
                 * add previous state to finalStates arraylist*/
                if (second.isEmpty()) {
                    finalStates.add(forArray1);
                    continue;
                }

                Collections.sort(second, new Comparator<Board>() {
                    public int compare(Board s1, Board s2) {
                        return s1.evaluate() - (s2.evaluate());
                    }
                });

                //Repeat 3 more times
                int minS = 6<second.size() ? 6 : second.size();
                for (int j = 0; j <minS; j++) {
                    Board forArray2 = second.get(j);
                    if (forArray2.gameOver()) {
                        finalStates.add(forArray2);
                        break;
                    }

                    ArrayList<Move> tempMoves3 = forArray2.possibleMoves(dice1, dice1, player);
                    for (int k = 0; k < tempMoves3.size(); k++) {
                        Board tempBoard = new Board(forArray2);
                        tempBoard.piecesOnBoard(tempMoves3.get(k).getFrom(), tempMoves3.get(k).getTo(), player);
                        third.add(tempBoard);
                    }

                    if (third.isEmpty()) {
                        finalStates.add(forArray2);
                        continue;
                    }

                    Collections.sort(third, new Comparator<Board>() {
                        public int compare(Board s1, Board s2) {
                            return s1.evaluate() - (s2.evaluate());
                        }
                    });

                    int minS2 = 7<third.size() ? 7 : third.size();
                    for (int k = 0; k <minS2; k++) {
                        Board forArray3 = third.get(k);

                        if (forArray3.gameOver()) {
                            finalStates.add(forArray3);
                            break;
                        }

                        ArrayList<Move> tempMoves4 = forArray3.possibleMoves(dice1, dice1, player);
                        for (int l = 0; l < tempMoves4.size(); l++) {
                            Board tempBoard = new Board(forArray3);
                            tempBoard.piecesOnBoard(tempMoves4.get(l).getFrom(), tempMoves4.get(l).getTo(), player);
                            fourth.add(tempBoard);
                        }

                        if (fourth.isEmpty()) {
                            finalStates.add(forArray3);
                            continue;
                        }

                        Collections.sort(fourth, new Comparator<Board>() {
                            public int compare(Board s1, Board s2) {
                                return s1.evaluate() - (s2.evaluate());
                            }
                        });

                        int minS3 = 15<fourth.size() ? 15 : fourth.size();
                        for (int l = 0; l <minS3; l++) {
                            Board forArray4 = fourth.get(l);
                            finalStates.add(forArray4);
                        }
                    }
                }
            }
        }

        //Add all the FINAL states in, check for duplicates, could use HashSet instead
        for(int size = 0; size < finalStates.size(); size++){
            if(!states.contains(finalStates.get(size)))
                states.add(finalStates.get(size));
        }

        return states;
    }

    //The heuristic we use to evaluate the positions of the pieces on the board
    public int evaluate() {

        int darkValue = 0;
        int lightValue = 0;
        int lsec = 0;
        int dsec = 0;
        int lastDark = 1;
        ArrayList<Integer> longestD = new ArrayList<>();
        ArrayList<Integer> longestL = new ArrayList<>();

        for(int i=24; i>0; i--) {
            //Dark player's pieces on board
            if(pieces[i].getNumber()>1 && pieces[i].getPlayer()==DARK) {
                lastDark = i > lastDark ? i : lastDark; 
                darkValue += 2;
                if(i!=1 && i<14 && pieces[i-1].getNumber()>1 && pieces[i-1].getPlayer()==DARK||(i!=24 && i<14 && pieces[i+1].getNumber()>1 && pieces[i+1].getPlayer()==DARK))
                    dsec++;
                if(i<9)
                    darkValue += 5;
                if(pieces[i].getNumber()>4)
                    darkValue-=(pieces[i].getNumber()-4);
                if(pieces[i].getNumber()>6)
                    darkValue-=(pieces[i].getNumber()-6);
            } else if(pieces[i].getNumber()==1 && pieces[i].getPlayer()==DARK) {
                lastDark = i > lastDark ? i : lastDark; 
                darkValue -= 2;
                if(i>17) darkValue+=4;
                if(i<10)
                    darkValue -= 17;
                if(!((i!=1 && pieces[i-1].getNumber()>1 && pieces[i-1].getPlayer()==DARK)||(i!=24 && pieces[i+1].getNumber()>1 && pieces[i+1].getPlayer()==DARK))) {
                    darkValue -= 2;
                }
                if(!((i!=24 && pieces[i+1].getNumber()>1 && pieces[i+1].getPlayer()==DARK)||((i!=1 && pieces[i-1].getNumber()>1 && pieces[i-1].getPlayer()==DARK)))) {
                    if(dsec>1) {
                        longestD.add(dsec);
                    }
                    dsec = 0;
                }
            } else {
                if(dsec>1) {
                    longestD.add(dsec);
                }
                 dsec = 0;
            }

            if(pieces[25].getNumber() !=0 )
                lastDark = 25;
            //Light player's
            if(pieces[i].getNumber()>1 && pieces[i].getPlayer()==LIGHT) {
                if(i!=1 && i>11 && pieces[i-1].getNumber()>1 && pieces[i-1].getPlayer()==LIGHT||(i!=24 && i>11 && pieces[i+1].getNumber()>1 && pieces[i+1].getPlayer()==LIGHT))
                    lsec++;
                if(i>16)
                    lightValue += 5;
                lightValue += 2;
                if(pieces[i].getNumber()>4)
                    lightValue-=(pieces[i].getNumber()-4);
                if(pieces[i].getNumber()>6)
                    lightValue-=(pieces[i].getNumber()-6);
            } else if(pieces[i].getNumber()==1 && pieces[i].getPlayer()==LIGHT&& i < lastDark) {
                if(i<8) lightValue+=4;
                if(i>15)
                    lightValue -= 17;
                if(!((i!=1 && pieces[i-1].getNumber()>1 && pieces[i-1].getPlayer()==LIGHT)||(i!=24 && pieces[i+1].getNumber()>1 && pieces[i+1].getPlayer()==LIGHT))) {
                    lightValue -= 3;
                }
                if(!((i!=24 && pieces[i+1].getNumber()>1 && pieces[i+1].getPlayer()==LIGHT)||(i!=1 && pieces[i-1].getNumber()>1 && pieces[i-1].getPlayer()==LIGHT))) {
                    if(lsec>1) {
                        longestL.add(lsec);
                    }
                    lsec = 0;
                }
            } else {
                if(lsec>1) {
                    longestL.add(lsec);
                }
                lsec = 0;
            }
        }

        //Award longer streaks of "safe" pieces
        for(int j = 0; j<longestD.size();j++){
            int temp = longestD.get(j);
            if(temp==2)
                darkValue += 2;
            else if(temp<5)
                darkValue += temp*1.5;
            else
                darkValue += temp*2.5;
        }

        for(int j = 0; j<longestL.size();j++){
            int temp = longestL.get(j);
            if(temp==2)
                lightValue += 2;
            else if(temp<5)
                lightValue += temp*1.5;
            else
                lightValue += temp*2.5;
        }

        if(pieces[0].getNumber()>0) {
            lightValue -= pieces[0].getNumber()*3;
            darkValue += pieces[0].getNumber()*4;
        }

        if(pieces[26].getNumber()>0) {
            lightValue += pieces[26].getNumber()*5;
        }

        if(pieces[25].getNumber()>0) {
            darkValue -= pieces[25].getNumber()*3;
            lightValue += pieces[25].getNumber()*4;
        }

        if(pieces[27].getNumber()>0) {
            darkValue += pieces[27].getNumber()*5;
        }

        return darkValue - lightValue;
    }
}
